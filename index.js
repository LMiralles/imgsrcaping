const puppeteer = require("puppeteer");

async function scrapeImages(url) {
  process.stdout.write("Chargement en cours...");
  const browser = await puppeteer.launch({
    executablePath: "C://Program Files/Google/Chrome/Application/chrome.exe",
  });

  const page = await browser.newPage();

  await page.goto(url);

  const data = await page.evaluate(() => {
    let src = [];
    document.querySelectorAll("img").forEach((element) => {
      src.push(element.src);
    });

    return src;
  });

  console.log(data);
}

process.stdout.write("Entrer l'url du site \n");
process.stdin.on("data", function (chunk) {
  scrapeImages(chunk.toString());
});
